import numpy as np
from numba import jit, stencil, prange, vectorize


def setup_3d(_q1, _q2, _q3, _k):
    '''
    Defines globally the variables that will be used within the kza
    algorithm.

    Parameters
    ----------
    _q1 : int
        Represents the half-window size for the first axis.
    _q2 : int
        Represents the half-window size for the second axis.
    _q3 : int
        Represents the half-window size for the third axis.
    k : int
        Represents how many times the filter will be applied.

    Returns
    -------
    None

    Notes
    -----
    This was done to the nature of numba.stencil that does not
    allow them as parameters
    
    '''
    global q1, q2, q3, k
    q1, q2, q3, k = _q1, _q2, _q3, _k


@jit(nopython=True, parallel=True)
def calculate_D1(ts):
    '''
    Returns the amplitude between the edges of the window.

    Parameters
    ----------
    ts : np.ndarray
        The 3D-array to be evaluated in the first axis.

    Returns
    -------
    D() : np.ndarray
        An 3D-array representing all the amplitudes over
        the first axis' windows' edges with size 2 * q1 + 1
        
    '''
    def D(a):
        return abs(a[q1, 0, 0] - a[-q1, 0, 0])
    return stencil(D, neighborhood=((-q1, q1), (0, 0), (0, 0)))(ts)


@jit(nopython=True, parallel=True)
def calculate_D2(ts):
    '''
    Returns the amplitude between the edges of the window.

    Parameters
    ----------
    ts : np.ndarray
        The 3D-array to be evaluated in the first axis.

    Returns
    -------
    D() : np.ndarray
        An 3D-array representing all the amplitudes over
        the second axis' windows' edges with size 2 * q2 + 1
        
    '''
    def D(a):
        return abs(a[0, q2, 0] - a[0, -q2, 0])
    return stencil(D, neighborhood=((0, 0), (-q2, q2), (0, 0)))(ts)


@jit(nopython=True, parallel=True)
def calculate_D3(ts):
    '''
    Returns the amplitude between the edges of the window.

    Parameters
    ----------
    ts : np.ndarray
        The 3D-array to be evaluated in the first axis.

    Returns
    -------
    D() : np.ndarray
        An 3D-array representing all the amplitudes over
        the third axis' windows' edges with size 2 * q3 + 1
        
    '''
    def D(a):
        return abs(a[0, 0, q3] - a[0, 0, -q3])
    return stencil(D, neighborhood=((0, 0), (0, 0), (-q3, q3)))(ts)


@jit(nopython=True, parallel=True)
def calculate_dD1(D):
    '''
    Returns the variation of amplitude between D1 points.

    Parameters
    ----------
    D : np.ndarray
        The 3D-array to be evaluated in the first axis.

    Returns
    -------
    dD() : np.ndarray
        An 3D-array representing all the variations of
        amplitudes over the first axis.

    Notes
    -----
    Same as in numpy.diff, but element-wise with numba.stencil.
        
    '''
    def dD(a):
        return a[1, 0, 0] - a[0, 0, 0]
    return stencil(dD)(D)


@jit(nopython=True, parallel=True)
def calculate_dD2(D):
    '''
    Returns the variation of amplitude between D2 points.

    Parameters
    ----------
    D : np.ndarray
        The 3D-array to be evaluated in the second axis.

    Returns
    -------
    dD() : np.ndarray
        An 3D-array representing all the variations of
        amplitudes over the second axis.

    Notes
    -----
    Same as in numpy.diff, but element-wise with numba.stencil.
        
    '''
    def dD(a):
        return a[0, 1, 0] - a[0, 0, 0]
    return stencil(dD)(D)


@jit(nopython=True, parallel=True)
def calculate_dD3(D):
    '''
    Returns the variation of amplitude between D3 points.

    Parameters
    ----------
    D : np.ndarray
        The 3D-array to be evaluated in the third axis.

    Returns
    -------
    dD() : np.ndarray
        An 3D-array representing all the variations of
        amplitudes over the third axis.

    Notes
    -----
    Same as in numpy.diff, but element-wise with numba.stencil.
        
    '''
    def dD(a):
        return a[0, 0, 1] - a[0, 0, 0]
    return stencil(dD)(D)


@vectorize(fastmath=True)
def vec_fd(v, m):
    '''
    Returns the fD*, weight array.

    This represents the weight of the amplitude in each given point
    being 0. the variation is too high, hence it'll be used to discard
    the window, or near 1. the window will be used almost fully.

    Parameters
    ----------
    v : np.ndarray
        The 3D-array D*, representing the amplitudes of the *-axis.
    m : float
        Max value of v, precalculated.
        
    Returns
    -------
    The weight value for each amplitude relative to the maximum global
    amplitude.
    
    '''
    return 1 - v / m


def relative_average_3d(ts, dD1, dD2, dD3, fD1, fD2, fD3):
    '''
    Returns the avg function applied to the given parameters.

    The avg function is wrapped with numba.stencil which allows it to work
    element-wise and over its neighbors, so it can make the proper mean
    calculations over the adaptative windows. These windows are decided by
    the dD* arrays which say if the windows are expanding or retracting, if
    they are expanding the tail of the window is prioritized, i.e. the first
    half of the window is used entirely, whereas the head of the window
    is used partially or not at all dependending on the fD* value given
    times the window size q*, and vice-versa in case the window variation
    is retracting. So the algorithm is applied to every axis and the mean
    of every element is calculated relative to the given windows.

    Parameters
    ----------
    ts : np.ndarray
        The 3D-array to be filtered.
    dD1 : np.ndarray
        A 3D-array representing the variation
        of amplitudes over the first axis,
        with such window has size 2 * q1 + 1.
    dD2 : np.ndarray
        A 3D-array representing the variation
        of amplitudes over the secoond axis,
        with such window has size 2 * q2 + 1.
    dD3 : np.ndarray
        A 3D-array representing the variation
        of amplitudes over the third axis,
        with such window has size 2 * q3 + 1.
    fD1 : np.ndarray
        A 3D-array representing the proportional value
        weight in the first axis: 1. - D1 / numpy.nanmax(D1).
    fD2 : np.ndarray
        A 3D-array representing the proportional value
        weight in the second axis: 1. - D2 / numpy.nanmax(D2).
    fD3 : np.ndarray
        A 3D-array representing the proportional value
        weight in the third axis: 1. - D3 / numpy.nanmax(D3).

    Returns
    -------
    avg() : np.ndarray
        The 3D-array filtered.

    Notes
    -----
    The parameters q1, q2, q3 and k are defined globally, so they're not
    needed to be parsed.
    Also the avg function is wrapped by this function because the params q*
    are not yet setup, only when the kza filter is called.

    '''
    @stencil(neighborhood=((-q1, q1), (-q2, q2), (-q3, q3)))
    def avg(a, dD1, dD2, dD3, fD1, fD2, fD3):
        soma = 0.
        div = 0
        t1 = q1 if dD1[0, 0, 0] > 0 else int(
            fD1[0, 0, 0] * q1) if not np.isnan(dD1[0, 0, 0]) else 0
        h1 = q1 if dD1[0, 0, 0] < 0 else int(
            fD1[0, 0, 0] * q1) if not np.isnan(dD1[0, 0, 0]) else 0

        t2 = q2 if dD2[0, 0, 0] > 0 else int(
            fD2[0, 0, 0] * q2) if not np.isnan(dD2[0, 0, 0]) else 0
        h2 = q2 if dD2[0, 0, 0] < 0 else int(
            fD2[0, 0, 0] * q2) if not np.isnan(dD2[0, 0, 0]) else 0

        t3 = q3 if dD3[0, 0, 0] > 0 else int(
            fD3[0, 0, 0] * q3) if not np.isnan(dD3[0, 0, 0]) else 0
        h3 = q3 if dD3[0, 0, 0] < 0 else int(
            fD3[0, 0, 0] * q3) if not np.isnan(dD3[0, 0, 0]) else 0
        
        for i in prange(-t1, h1 + 1):
            for j in prange(-t2, h2 + 1):
                for k in prange(-t3, h3 + 1):
                    var = a[i, j, k]
                    if not np.isnan(var):
                        soma += var
                        div += 1
        return soma / div if div != 0 else np.nan
    return avg(ts, dD1, dD2, dD3, fD1, fD2, fD3)


@jit(forceobj=True, parallel=True)
def filter_kza_3d(ts, _q1, _q2, _q3, _k):
    '''
    Returns a 3D-array that will be filtered accordingly
    to the Kolmogorov-Zurbenko filter described at
    Yang & Zurbenko, 2010.

    It expands the borders of the array accordingly to the
    windows sizes, so the original borders can be preserved
    and avoid a loss of data over the filtering process.

    Parameters
    ----------
    ts : np.ndarray
        The 3D-array to be filtered.
    _q1 : int
        Window size of the first axis.
    _q2 : int
        Window size of the second axis.
    _q3 : int
        Window size of the third axis.
    _z : int
        Number of times the filter will be applied over the given array.

    Returns
    -------
    ts : np.ndarray
        3D-array filtered with the same shape as the input.

    See Also
    --------
    relative_average_3d : Calculates the adaptative means.

    Examples
    --------
    >>> x = np.random.rand(10, 10, 1)
    >>> x_filtered = filter_kza_3d(x, 3, 3, 0, 3)
    
    '''
    setup_3d(_q1, _q2, _q3, _k)
    
    slice1 = slice(q1, -q1) if q1 != 0 else slice(None, None)
    slice2 = slice(q2, -q2) if q2 != 0 else slice(None, None)
    slice3 = slice(q3, -q3) if q3 != 0 else slice(None, None)
    
    for i in range(k):
        exp_shape = tuple([s + 2*e  for s, e in zip(ts.shape, [q1, q2, q3])])
        exp = np.full(exp_shape, np.nan)
        exp[slice1, slice2, slice3] = ts
        
        var_D1 = calculate_D1(exp)[slice1, slice2, slice3]
        var_D2 = calculate_D2(exp)[slice1, slice2, slice3]
        var_D3 = calculate_D3(exp)[slice1, slice2, slice3]
        
        var_dD1 = calculate_dD1(var_D1)
        var_dD2 = calculate_dD2(var_D2)
        var_dD3 = calculate_dD3(var_D3)
        
        var_dD1[-1, :, :] = np.nan
        var_dD2[:, -1, :] = np.nan
        var_dD3[:, :, -1] = np.nan
        
        local_max1 = np.nanmax(var_D1)
        local_max2 = np.nanmax(var_D2)
        local_max3 = np.nanmax(var_D3)

        var_fD1 = vec_fd(var_D1, local_max1)
        var_fD1[np.isnan(var_fD1)] = 0.
        var_fD2 = vec_fd(var_D2, local_max2)
        var_fD2[np.isnan(var_fD2)] = 0.
        var_fD3 = vec_fd(var_D3, local_max3)
        var_fD3[np.isnan(var_fD3)] = 0.

        var_dD1_exp = np.full(exp_shape, np.nan)
        var_dD1_exp[slice1, slice2, slice3] = var_dD1
        var_dD2_exp = np.full(exp_shape, np.nan)
        var_dD2_exp[slice1, slice2, slice3] = var_dD2
        var_dD3_exp = np.full(exp_shape, np.nan)
        var_dD3_exp[slice1, slice2, slice3] = var_dD3

        var_fD1_exp = np.full(exp_shape, np.nan)
        var_fD1_exp[slice1, slice2, slice3] = var_fD1
        var_fD2_exp = np.full(exp_shape, np.nan)
        var_fD2_exp[slice1, slice2, slice3] = var_fD2
        var_fD3_exp = np.full(exp_shape, np.nan)
        var_fD3_exp[slice1, slice2, slice3] = var_fD3
        
        ts = relative_average_3d(exp,
                                 var_dD1_exp,
                                 var_dD2_exp,
                                 var_dD3_exp,
                                 var_fD1_exp,
                                 var_fD2_exp,
                                 var_fD3_exp)[slice1, slice2, slice3]
    return ts
